D = .
include $(D)/Makefile.in

######################################
#	env variables
######################################

ifndef QEMU
QEMU := qemu
endif

######################################
#
######################################

INCLUDE_DIRS = \
	kernel/Makefile.in \
	lib/Makefile.in

include $(INCLUDE_DIRS)

OBJS := $(SRC:.S=.o)
OBJS := $(OBJS:.c=.o)
DEPS = $(OBJS:.o=.d)

all: kernel

-include $(DEPS)

kernel: $(OBJS)
	@mkdir -p bin
	@echo [$(LD)] bin/kernel
	@$(LD) -T link.ld -o bin/kernel $(OBJS) lib/string/string.O

string:
	@cd lib/string; make download; make compile;

clean-string:
	cd lib/string; make clean;

clean:
	find . -name "*.o" | xargs rm -f
	find . -name "*.d" | xargs rm -f

qemu: kernel
	$(QEMU) -kernel bin/kernel
