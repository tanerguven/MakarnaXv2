#include <string.h>
#include <types.h>
#include <asm/io.h>

static void cga_putc(int c);

extern void console_putc(int c) {
	cga_putc(c);
}

/*******************************************************************
 * 							CGA
 *******************************************************************/

#define CRTPORT 0x3d4

/*
 * 16 kb CGA memory
 * ekran 25 x 80 karakter, her karakter 16 bit (ekranda gorulen karakterler 4000 byte)
 * 16 kb ile 4 sayfa kullanilabilir
 * Burada sadece ekranda goruntulenen 4000 byte bellek kullaniliyor
 */
static uint16_t (*cga_memory)[80] = (uint16_t(*)[80])0xb8000;

#define TAB_SIZE 8
#define CHAR_COLOR 0x0700

static void cga_putc(int c) {
	int cursor_pos;

	// read cursor position
	outb(CRTPORT, 14);
	cursor_pos = inb(CRTPORT + 1) << 8;
	outb(CRTPORT, 15);
	cursor_pos |= inb(CRTPORT + 1);

	uint8_t cursor_x = cursor_pos % 80;
	uint8_t cursor_y = cursor_pos / 80;

	switch (c & 0xff) {
	case '\b': //backspace
		if (cursor_x | cursor_y) {
			if (cursor_x-- == 0) {
				cursor_x = 0;
				cursor_y--;
			}
			cga_memory[cursor_y][cursor_x] = (' ' & 0xff) | CHAR_COLOR;
		}
		break;
	case '\n':
		cursor_y++;
		cursor_x = 0;
		break;
	case '\t':
		cursor_x += TAB_SIZE - (cursor_x % TAB_SIZE);
		break;
	default:
		cga_memory[cursor_y][cursor_x] = (c & 0xff) | CHAR_COLOR;
		if (++cursor_x == 0) {
			cursor_x = 0;
			cursor_y++;
		}
	}

	if (cursor_y > 24) { // Scroll up
		memmove(&cga_memory[0][0], &cga_memory[1][0], sizeof(cga_memory[0]) * 25);
		cursor_y--;
		memset(&cga_memory[cursor_y][cursor_x], 0, sizeof(cga_memory[0]) - cursor_x);
	}

	cursor_pos = cursor_y * 80 + cursor_x;

	outb(CRTPORT, 14);
	outb(CRTPORT + 1, cursor_pos >> 8);
	outb(CRTPORT, 15);
	outb(CRTPORT + 1, cursor_pos);
	cga_memory[cursor_y][cursor_x] = ' ' | CHAR_COLOR;
}
